<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function data(Request $request)
    {
        if (empty($request->input('start'))) {
            $request->request->add([
                'start' => '0'
            ]);
        }
        if (empty($request->input('length')) || $request->input('length') === '-1') {
            $request->request->add([
                'length' => Post::all()->count()
            ]);
        }
        if (empty($request->input('search.value'))) {
            $request->request->add([
                'search' => [
                    'value' => '',
                    'regex' => 'false'
                ]
            ]);
        }
        $query = Post::query();
        if ($request->input('search.value')) {
            $query->where('title', 'like', '%' . $request->input('search.value') . '%');
        }
        $data = $query->orderBy($request->input('columns.' . $request->input('order.0.column') . '.data') ? $request->input('columns.' . $request->input('order.0.column') . '.data') : 'id', $request->input('order.0.dir') === 'asc' ? 'asc' : 'desc')
            ->skip(intval($request->input('start')))
            ->take(intval($request->input('length')))
            ->get()
            ->toArray();
        $result = [
            'draw' => intval($request->input('draw', 1)),
            'recordsTotal' => intval(Post::all()->count()),
            'recordsFiltered' => intval($query->count()),
            'data' => empty($data) ? [] : $data
        ];
        die(json_encode($result));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.Post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = DB::table('categories')->leftJoin('cate_type','categories.type','cate_type.id')
            ->where('cate_type.name','Category')->select('categories.id','categories.name')->get();
        $tag = DB::table('categories')->leftJoin('cate_type','categories.type','cate_type.id')
            ->where('cate_type.name','Tag')->select('categories.id','categories.name')->get();
        return view('Admin.Post.create',compact('category','tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->title        = $request->title;
        $post->metaTitle    = $request->meta_title;
        $post->slug         = $request->title;
        $post->content      = $request->content;
        if ($request->published != 'on'){
            $post->published    = 'off';
        }
        else{
            $post->published = $request->published;
        }
        if($request->img_path){
            $post->img_name     = $request->img_path;
            $post->img_path     = 'assets/uploads/' . $request->img_path;
        }
        $post->created_by   = Auth::user()->name;
        $post->updated_by   = Auth::user()->name;
        $post->save();
        if($request->category){
            DB::table('post_meta')->insert(array('post_id' => $post->id, 'cate_id' => $request->category));
        }
        if(is_array($request->tag) && !empty($request->tag)){
            foreach ($request->tag as $value){
                DB::table('post_meta')->insert(array('post_id' => $post->id, 'cate_id' => $value));
            }
        }
        return redirect()->route('post.index')->with('message',__('messages.post').' '.__('messages.create').' '.__('messages.successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post,Request $request)
    {
        $category = DB::table('categories')->leftJoin('cate_type','categories.type','cate_type.id')
            ->where('cate_type.name','Category')->select('categories.id','categories.name')->get();
        $tag = DB::table('categories')->leftJoin('cate_type','categories.type','cate_type.id')
            ->where('cate_type.name','Tag')->select('categories.id','categories.name')->get();
        $check_cate = DB::table('post_meta')->where('post_id', $post->id)->select('cate_id')->pluck('cate_id')->toArray();
        return view('Admin.Post.edit',compact('post','category','tag','check_cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title        = $request->title;
        $post->metaTitle    = $request->meta_title;
        $post->slug         = $request->title;
        $post->content      = $request->content;
        $post->published    = 'on';
        if ($request->published != 'on'){
            $post->published    = 'off';
        }
        if($request->img_path){
            $post->img_name     = $request->img_path;
            $post->img_path     = 'assets/uploads/' . $request->img_path;
        }
        $post->created_by   = Auth::user()->name;
        $post->updated_by   = Auth::user()->name;

        $post->update();
        DB::table('post_meta')
            ->leftJoin('categories','cate_id','categories.id')
            ->leftJoin('cate_type','cate_type.id','categories.id')
            ->where('post_meta.post_id',$post->id)->delete();
        if($request->category){
            DB::table('post_meta')
                ->leftJoin('categories','cate_id','categories.id')
                ->leftJoin('cate_type','cate_type.id','categories.id')
                ->where('cate_type.name', 'Category')
                ->where('post_id',$post->id)->insert(array('post_id' => $post->id, 'cate_id' => $request->category));
        }
        if(is_array($request->tag) && !empty($request->tag)){
            foreach ($request->tag as $value){
                DB::table('post_meta')
                    ->leftJoin('categories','cate_id','categories.id')
                    ->leftJoin('cate_type','cate_type.id','categories.id')
                    ->where('cate_type.name', 'Tag')->where('post_id',$post->id)
                    ->insert(array('post_id' => $post->id, 'cate_id' => $value));
            }
        }
        return redirect()->route('post.index')->with('message',__('messages.post').' '.__('messages.updated').' '.__('messages.successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        DB::table('post_meta')->where('post_id',$post->id)->delete();
        return redirect()->route('post.index')->with('message',__('messages.post').' '.__('messages.deleted').' '.__('messages.successfully'));
    }
}
