@extends('layouts.backend')
@section('content')
<style>
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 100%;
    }
    img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }
    .fancybox-slide--iframe .fancybox-content {
        width  : 800px;
        height : 600px;
        max-width  : 80%;
        max-height : 80%;
        margin: 0;
    }
</style>
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{ __('messages.list') }} {{ __('messages.post') }}</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('messages.home') }}</a></li>
            <li class="breadcrumb-item active">{{ __('messages.post') }} </li>
        </ol>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i>
        @foreach ($errors->all() as $error)
        {{ $error }}
        @endforeach
    </h5>
</div>
@endif
<form method="post" action="{{ route('post.store') }}" enctype="multipart/form-data">
    <div class="row">
        <!-- left column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.add_new') }} {{ __('messages.post') }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">{{ __('messages.title') }}</label>
                        <abbr title="required">*</abbr>
                        <input type="text" class="form-control" id="name" name="title" onkeyup="ChangeToSlug();" maxlength="100" placeholder="{{ __('messages.title') }}">
                        <em> {{ __('messages.note_title') }}</em>
                    </div>
                    <div class="form-group">
                        <label for="meta_title">{{ __('messages.meta_title') }}</label>
                        <abbr title="required">*</abbr>
                        <textarea type="text" class="form-control" id="meta_title" name="meta_title" onkeyup="ChangeToSlug();" maxlength="150" placeholder="{{ __('messages.meta_title') }}"></textarea>
                        <em> {{ __('messages.note_meta_title') }}</em>
                    </div>
                    <div class="form-group">
                        <label for="content">{{ __('messages.content') }}</label>
                        <textarea type="text" class="form-control" name="content" id="description" placeholder="{{ __('messages.content') }}"></textarea>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save</button>
                    <button type="reset" class="btn btn-success"><i class="fas fa-redo-alt"></i> Reset</button>
                    <button type="button" class="btn btn-danger" onclick="javascript:window.location='{{ route('post.index') }}';"><i class="fas fa-sign-out-alt"></i> Cancel</button>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-4">
            <!-- general form elements disabled -->
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.static_path') }}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <label>{{ __('messages.slug') }}</label>
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="{{ __('messages.slug') }}" disabled="">
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.categories') }}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <label>{{ __('messages.category') }}</label>
                        <div class="input-group">
                            <select class="custom-select" name="category">
                                @foreach($category as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <label>{{ __('messages.published') }}</label>
                        <div class="bootstrap-switch-container">
                            <span class="bootstrap-switch-label" style="width: 43.2px;">&nbsp;</span>
                            <input type="checkbox" checked="" data-bootstrap-switch="" name="published" data-off-color="danger" data-on-color="success">
                            <em> {{ __('messages.note_published') }}</em>
                        </div>
                    </div>
                    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
                    <div class="row">
                        <label>{{ __('messages.tag') }}</label>
                        <div class="input-group">
                            <label for="tag"></label>
                            <select id="tag" name="tag[]" multiple="multiple">
                                @foreach($tag as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="card card-light">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.avatar') }}</h3>
                </div>
                <!-- /.card-body -->
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <div class="card-body">
                    <div class="row">
                        <label for="img_path">{{ __('messages.avatar') }}</label>
                        <div class="input-group">
                            <input type="text" name="img_path" id="image-input" class="form-control" readonly>
                            <span class="input-group-append">
                                    <button href="{{ asset('/assets/filemanager/dialog.php?relative_url=1&type=1&field_id=image-input') }}"
                                            name="img_path"  type="button" id="avatar" class="btn  btn-primary iframe-btn" >{{ __('messages.upload_avatar') }}</button>
                                </span>
                        </div>
                    </div><br>
                    <div class="row">
                        <img id="avatar_img" class="image-preview" onClick="swipe();"  width="100%" height="50%" border="1px">
                    </div>
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
</form>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#avatar_img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#avatar").change(function() {
        readURL(this);
    });
    $(document).ready(function() {
        $('#tag').select2({
            theme: 'bootstrap4',
        });
    });
</script>
<script type="text/javascript">
    $("document").ready(function(){
        setTimeout(function(){
            $("div.alert").remove();
        }, 2000 ); // 2 secs

    });
</script>
{{--config select avatar--}}
<script type="text/javascript">
    $('.iframe-btn').fancybox({
        'type'		: 'iframe',
        'autoScale'    	: false,

    });
    function swipe() {
        var largeImage = document.getElementById('avatar_img');
        var url=largeImage.getAttribute('src');
        window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
    }
    function responsive_filemanager_callback(field_id){
        console.log(field_id);
        var url=jQuery('#'+field_id).val();
        $('.image-preview').attr('src','{{ asset('/assets/uploads/') }}' +'/'+ url);
    }
    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
</script>
<script>
    // Date range
    $(function() {
        $('input[name="created_at"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY',
            }
        });
    });
</script>
@endsection
