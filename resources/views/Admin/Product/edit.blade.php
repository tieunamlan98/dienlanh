@extends('layouts.backend')
@section('content')
    <style>
        img {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 100%;
        }
        img:hover {
            box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
        }
        .fancybox-slide--iframe .fancybox-content {
            width  : 800px;
            height : 600px;
            max-width  : 80%;
            max-height : 80%;
            margin: 0;
        }
        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }
        .green{
            background-color:#6fb936;
        }
        .thumb{
            margin-bottom: 30px;
        }
        .page-top{
            margin-top:85px;
        }
        img.zoom {
            width: 90px;
            height: 90px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }
        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }
        .modal-header {

            border-bottom: none;
        }
        .modal-title {
            color:#000;
        }
        .modal-footer{
            display:none;
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all"
          rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all"
          rel="stylesheet" type="text/css">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ __('messages.list') }} {{ __('messages.product') }}</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">{{ __('messages.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('messages.product') }} </li>
            </ol>
        </div>
        <!-- /.col -->
    </div>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i>
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </h5>
        </div>
    @endif
    <form method="post" action="{{ route('product.update',$product->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <!--/.col (left) -->
            <div class="col-md-8">
                <!-- /.card -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title">{{ __('messages.add_new') }} {{ __('messages.product') }}</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{ __('messages.product_name') }}</label>
                            <abbr title="required">*</abbr>
                            <input type="text" class="form-control" id="name" name="name" onkeyup="ChangeToSlug();" placeholder="{{ __('messages.cate_name') }}" value="{{$product->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="description">{{ __('messages.description') }}</label>
                            <abbr title="required">*</abbr>
                            <textarea type="text" class="form-control" name="description" id="description" placeholder="{{ __('messages.description') }}">{{$product->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="gallery">{{ __('messages.gallerys') }}</label>
                            <input type="file" id="file-1" name="gallery[]" multiple class="file" data-overwrite-initial="false">
                        </div>

                    </div>
                    <!-- /.card-footer -->
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{ __('messages.save') }}</button>
                        <button type="reset" class="btn btn-success"><i class="fas fa-redo-alt"></i> {{ __('messages.reset') }}</button>
                        <button type="button" class="btn btn-danger" onclick="javascript:window.location='{{ route('product.index') }}';"><i class="fas fa-sign-out-alt"></i> {{ __('messages.cancel') }}</button>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
            <div class="col-md-4">
                <div class="card card-light">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title">{{ __('messages.static_path') }}</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <label>{{ __('messages.slug') }}</label>
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="{{ __('messages.slug') }}" disabled="" value="{{$product->slug}}">
                        </div>
                    </div>
                </div>
                <div class="card card-light">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title">{{ __('messages.category') }}</h3>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                            <label for="quantity">{{ __('messages.category') }}</label>
                            <abbr title="required">*</abbr>
                            <select class="form-control" id="category_id" name="category_id">
                                @foreach($category as $item)
                                    <option value="{{$item->id }}" @if($product->category_id == $item->id) selected @endif>{{$item->name }}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-md-6">
                                <label for="quantity">{{ __('messages.quantity') }}</label>
                                <abbr title="required">*</abbr>
                                <input type="text" class="form-control"  value="{{ $product->quantity }}" name="quantity" id="quantity" placeholder="{{ __('messages.quantity') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="price">{{ __('messages.price') }}</label>
                                <abbr title="required">*</abbr>
                                <input type="text" class="form-control"  id="price" name="price" value="{{ $product->price }}" placeholder="{{ __('messages.price') }}">
                            </div>
                            <div class="col-md-4">
                                <label for="price">{{ __('messages.unit') }}</label>
                                <input type="text" class="form-control" placeholder="{{ __('VNĐ') }}" readonly >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-light">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title">{{ __('messages.avatar') }}</h3>
                    </div>
                    <!-- /.card-body -->
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                    <div class="card-body">
                        <div class="row">
                            <label for="avatar">{{ __('messages.avatar') }}</label>
                            <div class="input-group">
                                <input type="text" name="avatar" placeholder="{{ $product->name_avatar }}"
                                       value="{{ $product->name_avatar }}" id="image-input" class="form-control" readonly>
                                <span class="input-group-append">
                                    <button href="{{ asset('/assets/filemanager/dialog.php?relative_url=1&type=1&field_id=image-input') }}" name="img_path"  type="button" id="avatar" class="btn  btn-primary iframe-btn" >{{ __('messages.upload_avatar') }}</button>
                                </span>
                            </div>
                        </div><br>
                        <div class="row">
                            @if($product->path_avatar == null)
                                <em>{{ __('messages.avatar_null') }}</em>
                            @else
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch3" name="delete_avatar" >
                                    <label class="custom-control-label" for="customSwitch3">{{ __('messages.delete_avatar') }}</label>
                                </div>
                                <img class="image-preview" onclick="swipe()" src="{{ asset($product->path_avatar) }}" id="avatar_img" width="100%" height="50%" border="1px">
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @if($gallery == null)

                            @else
                                <div class="input-group">
                                    <label for="gallery">{{ __('messages.gallerys') }}</label>
                                </div><br>

                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="delete_gallery" name="delete_gallery" >
                                    <label class="custom-control-label" for="delete_gallery">{{ __('messages.delete_gallery') }}</label>
                                </div>
                                <div class="input-group" style="border: 1px solid #e0e0e0; padding: 15px; background-image: url({{ asset('/img/img_gallery.jpg') }});">
                                    @foreach($gallery as $image)
                                        <a href="{{ asset( $image->cover_img) }}" class="fancybox" rel="ligthbox">
                                            <img class="zoom img-fluid" src="{{ asset( $image->cover_img) }}" alt="image">
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $(".fancybox").fancybox({
                                openEffect: "none",
                                closeEffect: "none"
                            });

                            $(".zoom").hover(function(){

                                $(this).addClass('transition');
                            }, function(){

                                $(this).removeClass('transition');
                            });
                        });

                    </script>
                </div>
            </div>
        </div>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#file-1").fileinput({
            theme: 'fa',
            showUpload:false,
            showCancel:false,
            allowedFileExtensions:['jpeg','png','jpg','gif','svg'],
            overwriteInitial:false,
            maxFileSize:2048,
            maxFileNum:4,
            slugCallback:function (filename){
                return filename.replace('(','_').replace(']','_');
            }
        });
    </script>
{{--    config validate input text allow only numberic--}}
    <script type="text/javascript">
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            });
        }
        setInputFilter(document.getElementById("quantity"), function(value) { return /^-?\d*$/.test(value); });
        setInputFilter(document.getElementById("price"), function(value) { return /^-?\d*$/.test(value); });

    </script>
    {{--select2--}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#category_id').select2({
                theme: 'bootstrap4',minimumInputLength: 2,
            });
        });
    </script>
    <script type="text/javascript">
        $('.iframe-btn').fancybox({
            'type'		: 'iframe',
            'autoScale'    	: false
        });
        function swipe() {
            var largeImage = document.getElementById('avatar_img');
            var url=largeImage.getAttribute('src');
            window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
        }
    </script>
    <script type="text/javascript">
        function responsive_filemanager_callback(field_id){
            var url=jQuery('#'+field_id).val();
            $('.image-preview').attr('src','{{ asset('/assets/uploads/') }}' +'/'+ url);
        }
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#avatar").change(function(){
            readURL(this);
        });
    </script>
@endsection
